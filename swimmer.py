# Copyright (C) 2022 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Stevens Paz

from collections import namedtuple
from typing import Union

import numpy as np
import pandas as pd


def reward_displacement(seq_data: pd.DataFrame):
    return seq_data.loc[:, "center_of_mass_displacement"]


def reward_intake_increase(seq_data: pd.DataFrame):
    end_cols = [x for x in seq_data.columns if "end_of_action" in x]
    ini_cols = [x for x in seq_data.columns if "start_of_action" in x]
    return (seq_data.loc[:, end_cols].sum(axis=1)
            - seq_data.loc[:, ini_cols].sum(axis=1))


def reward_intake_accumulation(seq_data: pd.DataFrame, *, dt=0.1):
    accum_cols = [x for x in seq_data.columns if "accumulated_food_uptake" in x]
    end_cols = [x for x in seq_data.columns if "end_of_action" in x]
    ini_cols = [x for x in seq_data.columns if "start_of_action" in x]
    return (seq_data.loc[:, accum_cols].sum(axis=1)
            - 0.5 * seq_data.loc[:, end_cols+ini_cols].sum(axis=1)) * dt


def compute_rewards(df: pd.DataFrame, *, rewards: dict):
    """Compute rewards in place."""
    for rwd_col, rwd in rewards.items():
        df[rwd_col] = rwd(df)
    return df


SwimmingGait = namedtuple("SwimmingGait", ["state_seq", "action_seq"])
"""Description of a swimming gait in terms of it state and actions sequences"""

# Rotation equivalent state sequence (starts with the lowest state)
swim_to_right = SwimmingGait(state_seq=np.array([0, 2, 3, 1]), action_seq=np.array([0, 1, 0]))


def contract_link(state: Union[int, np.ndarray], link: Union[int, np.ndarray]) -> Union[int, np.ndarray]:
    """ Contract link in current state and return next state."""
    # applying XOR operator on state and action + 1
    # FIXME remap actions to simplify this operation
    link = 1 - link  # 0:right, 1:left

    link = np.atleast_1d(link) + 1
    state = np.atleast_1d(state)
    retval = np.bitwise_xor(state, link)
    if retval.size == 1:
        retval = retval[0]
    return retval


def reward_linear_combination(seq_data: pd.DataFrame, w: np.ndarray) -> pd.Series:
    cols = [x for x in seq_data.columns if "uptake" in x]
    return seq_data.loc[:, cols].dot(w)
