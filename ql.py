# Copyright (C) 2024 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Stevens Paz

from typing import Union, Callable

import numpy as np
import pandas as pd


def runQL(seq_data: pd.DataFrame, *,
          alpha: float, gamma: float, reward: str,
          steps: int = None,
          current_state_col: str = "current_state",
          next_state_col: str = "next_state",
          action_col: str = "action",
          ):
    """Run Q-learning in a given sequence data """
    steps = seq_data.shape[0] if steps is None else steps
    # Initilize QL matrix
    states = np.sort(seq_data[current_state_col].unique())
    actions = np.sort(seq_data[action_col].unique())
    MQL = np.zeros((states.size, actions.size))

    # QL steps
    for idx in seq_data.index[:steps]:
        prev_state_dec = seq_data.loc[idx, current_state_col]
        next_action_dec = seq_data.loc[idx, action_col]
        next_state_dec = seq_data.loc[idx, next_state_col]

        next_MQLmax = np.max(MQL[next_state_dec, :])
        current_MQLval = MQL[prev_state_dec, next_action_dec]
        current_reward = seq_data.loc[idx, reward]

        MQL[prev_state_dec, next_action_dec] = (1 - alpha) * current_MQLval + \
                                               alpha * (current_reward + gamma * next_MQLmax)
    MQL_df = pd.DataFrame(index=states, columns=actions, data=MQL)
    MQL_df.index.name = "state"
    MQL_df.columns.name = "action"
    return MQL_df


def policy_from_mql(mql: Union[np.ndarray, pd.DataFrame]) -> Union[np.ndarray, pd.DataFrame]:
    """Extract greedy policy from Q-learning matrix."""
    if isinstance(mql, pd.DataFrame):
        policy = mql.agg(np.argmax, axis=1)
        policy = pd.DataFrame(policy, columns=["action"])
    else:
        policy = np.argmax(mql, axis=1)
    return policy


def policy_to_transition(policy: Union[np.ndarray, pd.DataFrame],
                         action_fcn: Callable) -> Union[np.ndarray, pd.DataFrame]:
    """Convert state-action policy to current-next state transition."""
    if isinstance(policy, pd.DataFrame):
        trn = pd.DataFrame(action_fcn(policy.index, policy.action),
                           index=policy.index)
        trn.rename(columns={0: "next_state"}, inplace=True)
    elif isinstance(policy, np.ndarray):
        trn = action_fcn(np.arange(policy.size), policy)
    else:
        raise ValueError("Unknown policy type.")
    return trn


def gait_from_policy(policy: Union[np.ndarray, pd.DataFrame],
                     *, state0: int, action_fcn: Callable,
                     max_length: int = 100,
                     standarize: bool = True) -> np.ndarray:
    """Convert policy to gait (rotation equivalent if standarize)."""
    trn = policy_to_transition(policy, action_fcn)
    if isinstance(trn, np.ndarray):
        trn = pd.DataFrame(columns={"next_state": trn})
        trn.index.name = "state"

    gait_ = [state0, ]
    for i in range(1, max_length):
        ns = trn.loc[gait_[-1]][0]
        if ns in gait_:
            # We have seen this state (and its transition)
            if ns != state0:
                # If it is not the initial one, add it so the periodicity is visible.
                # initial state was "garden of eden"
                gait_.append(ns)
                if standarize:
                  # "garden of eden" state is not part of the gait, hence
                  # we eliminate all states in the gait until the first occurents 
                  # of current ns, inclusively
                  gait_ = gait_[gait_.index(ns)+1:]
            break
        gait_.append(ns)
    gait = rotation_equivalent(gait_) if standarize else np.asarray(gait_)
    return gait


def gait_from_mql(mql: Union[np.ndarray, pd.DataFrame], **kwargs) -> np.ndarray:
    """Convert QL matrix to gait (rotation equivalent if standarize)."""
    policy = policy_from_mql(mql)
    return gait_from_policy(policy, **kwargs)


# !! Deprecated
def identify_final_gait(mql, initial_state, *, gait_period=4):
    slist = []
    alist = []
    current_state_dec = initial_state
    nlinks = mql.columns.size
    for j in range(gait_period):
        action = np.zeros(nlinks, dtype=int)
        aux_current = np.array([int(i) for i in np.binary_repr(current_state_dec, width=nlinks)])
        action[np.argmax(mql.loc[current_state_dec, :])] = 1
        aux_state = np.remainder(aux_current + action, 2 * np.ones(nlinks, dtype=int))
        next_state_dec = np.dot(aux_state, 2 ** np.arange(nlinks - 1, -1, -1, dtype=int))

        slist.append(current_state_dec)
        alist.append(action)

        current_state_dec = next_state_dec

    # FIXME action list is not an array of actions. Might be binary representation
    return slist, alist


def is_equal_rotation_invariant(s1: list, s2: list):
    """Check if two lists are equal disregarding rotations."""
    # align the sequences
    try:
        idx = s1.index(s2[0])
    except ValueError:
        return False
    s2_ordered = np.roll(s2, idx)
    return all([x == y for x, y in zip(s2_ordered, s1)])


def rotation_equivalent(s: Union[np.ndarray, list]) -> np.ndarray:
    """Generate a rotation equivalent list, with smallest element first"""
    if isinstance(s, list):
        idx = s.index(min(s))
    elif isinstance(s, np.ndarray):
        idx = s.argmin()
    return np.roll(s, -idx)


def reduce_gait(g: list):
    # detect first repeated element
    for idx, s in enumerate(g):
        if s in g[idx+1:]:
            break
    if idx == len(g)-1:
        return g
    return g[idx+1:]
