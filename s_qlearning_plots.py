#!/usr/bin/env python3

# Copyright (C) 2022 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
import sys
from pathlib import Path

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt

import ql

plt.rcParams['text.usetex'] = True
matplotlib.rcParams['font.size'] = 16
matplotlib.rcParams['figure.figsize'] = (15, 10)
matplotlib.rcParams['font.family']='serif'
matplotlib.rcParams['font.sans-serif'] = 'Helvetica'

reward_names = {
    "displacement": "Displacement",
    "intake_increase": "Intake increase",
    "intake_accumulation": "Intake accumulation"
    }


def draw_heatmap(*args, **kwargs):
    """Wrangle data and plot a heatmap."""
    data = kwargs.pop('data')
    d = data.loc[:, ["alpha", "gamma", "learned"]]
    # Normalization assuming number of batches per trajectory is constant
    n_norm = data.TrID.nunique() * data.batch.nunique()
    d = (100 * d.groupby(["alpha", "gamma"]).sum() / n_norm).reset_index()
    d = d.pivot("alpha", "gamma", "learned")
    ax = sns.heatmap(d, **kwargs)
    ax.invert_yaxis()


def maximize_figure():
    """Resize the current figure to screen size. """
    mng = plt.get_current_fig_manager()
    try:
        mng.resize(*mng.window.maxsize())
    except AttributeError:
        # If there is no window, don't do anything
        pass

# Name of the column with the action selector parameter
action_sel_parameter = "prob_action_uniform"

if __name__ == "__main__":

    filename = Path(sys.argv[1])  # First argument is the path to data file
    if filename.suffix == ".csv":
        df = pd.read_csv(filename)
    elif filename.suffix == ".pkl":
        df = pd.read_pickle(filename)
        # the following commented code oly works when there is a single trajectory
        #if "batch" in df:  
        #    df.rename(columns={"batch": "TrID"}, inplace=True)
        df[action_sel_parameter] = 0.5
        df.drop(columns=["action", "state", "Q_value"], inplace=True)
        df = df.drop_duplicates().reset_index(drop=True)

    # Data wrangle
    df.replace(to_replace={"reward": reward_names}, inplace=True)
    df["reward"] = df.reward.astype("category")
    # Not needed for ultiple trajectories
    #if action_sel_parameter not in df:
        # This collumn was called TrID in the  first version of the data
        # df[action_sel_parameter] = df.TrID
        # Trajectory Id is the same for same Pe
        # df["TrID"] = -1
        # for n, Pe_data in enumerate(df.groupby("Pe")):
        #    _, data = Pe_data
        #    df.loc[data.index, "TrID"] = n
    df["TrID"] = df.TrID.astype("category")
    #df["learned"] = df.learned.astype(int)

    # Plots
    # Heatmap

    n_trajectories = len(df.TrID.unique())  # FIXME: fix this when TrID is an 
                                            # actual ID
    
    #trajectory_parameter = 0.5
    for trajectory_parameter, df_traj in df.groupby(action_sel_parameter):
        # Same as looping over unique values of column action_sel_parameter:
        #
        # for trajectory_parameter in df[action_sel_parameter].unique():
        #     df_traj = df[df[action_sel_parameter] == trajectory_parameter]
        #
        g = sns.FacetGrid(df_traj, col="reward", row="Pe", margin_titles=True,
                          col_order=["Displacement", "Intake accumulation", "Intake increase"])
        #g.fig.suptitle(r"P($a_n \sim \mathcal{U}\left(\lbrace L, R \rbrace\right)$) = " + f"{trajectory_parameter}", fontsize=12)


        g.map_dataframe(draw_heatmap, annot=True, linewidths=.5, linecolor="lightgrey",
                        cbar=False, cmap="Blues", vmin=0, vmax=100, fmt=".0f")

        g.set_axis_labels("", "")
        g.fig.supxlabel(r"$\gamma$") 
        g.fig.supylabel(r"$\alpha$")
        g.fig.tight_layout()

    # Line of fraction learned for Pe
    fig = plt.figure()
    #idx_col = [action_sel_parameter, "Pe", "reward"]
    idx_col = ["TrID", "Pe", "reward"]
    records = []
    for TrId_Pe_R, data in df.groupby(idx_col).learned:
        record = dict(zip(idx_col, TrId_Pe_R))
        record[r"P(learn)"] = data.sum() / data.size
        record[r"P(learn)"] += 1e-6 * np.random.randn()  # Add noise to avoid bug in boxplot
        records.append(record)
    tl = pd.DataFrame.from_records(records)

    ax = sns.boxplot(data=tl, x="Pe", y="P(learn)", hue="reward", whis=np.inf, fliersize=0.0)
    ax = sns.stripplot(data=tl, x="Pe", y="P(learn)", hue="reward", dodge=True,
                       linewidth=1)
    # unique labels
    hand, labl = ax.get_legend_handles_labels()
    ax.legend(hand[:3], labl[:3], loc="upper right")
    #plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    fig.tight_layout()


    ## Count of different gaits over all alpha,gamma
    ## FIXME: this assumes we have the gaits that are not standaried (redcued and rotation equivalent).
    ##        If that's not the case comment the following lines
    def _stdp1(x: str):
        """standard gait plus one to match article notation."""
        # standarize and add one
        y = ql.rotation_equivalent(ql.reduce_gait(list(map(int, x)))) + 1
        # convert to string
        return "".join(map(str, y))

    df["standard_gait"] = df.gait.transform(_stdp1)
    # count occurences of standard gaits
    gc = df.groupby(["reward", "Pe"]).standard_gait.value_counts()
    gc.name="count"
    gc_df = gc.reset_index().rename(columns={"standard_gait": "gait"})
    sns.catplot(kind="bar", data=gc_df, x="gait", y="count", 
    hue="Pe", dodge=True, col="reward")

    #plt.ion()
    plt.show()


