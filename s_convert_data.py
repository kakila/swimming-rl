#!/usr/bin/env python3

# Copyright (C) 2022 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import pandas as pd

filename = "evolqlquantities.txt"
CSV_fmt = dict(
    header=None,
    sep=" ", 
    )

column_names = [
    "current state",
    "action",
    "next state",
    "center of mass displacement",
    "food uptake at the end of action sphere 1",
    "food uptake at the end of action sphere 2",
    "food uptake at the end of action sphere 3",
    "accumulated food uptake along action sphere 1",
    "accumulated food uptake along action sphere 2",
    "accumulated food uptake along action sphere 3",
]


if __name__ == "__main__":

    df = pd.read_csv(filename, **CSV_fmt, names=[x.replace(" ", "_") for x in column_names])
    food_cols = df.columns[df.columns.str.startswith("accumulated")]
    df["total_food_uptake"] = df[food_cols].sum(axis=1)
    df.index.name = "iteration"
    df.to_hdf("swimming-RL-2022-04-14.hdf", key="date_2022_04_14")

