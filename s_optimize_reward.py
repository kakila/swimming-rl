#!/usr/bin/env python3

# Copyright (C) 2022 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
from typing import Callable, Tuple
import sys
from pathlib import Path
from itertools import product
from multiprocessing import cpu_count
from concurrent.futures import ProcessPoolExecutor
from argparse import ArgumentParser
from functools import partial

import pandas as pd
import numpy as np
import scipy.optimize as sco
import seaborn as sns
import matplotlib.pyplot as plt

from ql import runQL, gait_from_mql
from sequence_filehandling import load_sequence_from_params
from swimmer import swim_to_right, contract_link, reward_linear_combination
from utils import overlapping_window_bounds


def parse_arguments(argv=None):
    """ Read arguments from command line. """
    parser = ArgumentParser()
    parser.description = "Optimize reward for chemotaxis at given Pe."
    parser.add_argument('--peclet', '-pe', type=float, default=None, required=True,
                        help="Péclet number")
    parser.add_argument('--action_random', '-arp', type=float, default=0.5,
                        help="Probability of next action drawn from the uniform distribution over actions.")
    parser.add_argument('--datapath', '-p', type=Path, default='.',
                        help="Path to the folder containing sequence files")
    parser.add_argument('--noparallel', action='store_true',
                           help='Do not run loops in parallel.')
    parser.add_argument('--alpha_range', '-a', type=float, nargs=3, default=[0.1, 1.0, 0.1],
                        help="initial, final, and step for QL parameter alpha.")
    parser.add_argument('--gamma_range', '-g', type=float, nargs=3, default=[0.1, 1.0, 0.1],
                        help="initial, final, and step for QL parameter gamma.")
    parser.add_argument('--window', '-w', type=int, nargs=3, default=[50, 50, 500],
                        help="initial offset, length, and offset between windows for the generation of batches from long sequences.")
    parser.add_argument('--data_length', '-d', type=int, default=None,
                        help="Data length to use. Data will be shorten if this is smaller than its length. Defaults to use all data.")

    if argv is None:
        argv = sys.argv[1:]
    args = parser.parse_args(argv)

    if not args.datapath.exists():
        raise ValueError(f"Path {args.datapath} not found.")

    return args


def gait_distance(g1, g2):
    return np.sum([u != v for u, v in zip(g1, g2)]) + np.abs(g1.size - g2.size)


def reward_local_cost(d, rwd, gait, a_g) -> int:
    """Encapsulation of the analysis for a given batch. """
    a, g = a_g
    q = runQL(d, reward=rwd, alpha=a, gamma=g)
    g = gait_from_mql(q, state0=swim_to_right.state_seq[0],
                      action_fcn=contract_link)
    return 1.0 - np.array_equal(g, gait)
    #return gait_distance(g, gait)


def reward_cost(x, *, data: pd.DataFrame, reward: Callable, alpha: np.ndarray, gamma: np.ndarray,
                windows: Tuple[np.ndarray, np.ndarray], gait: np.ndarray) -> float:
    rwd_name = "reward_fcn"
    data[rwd_name] = reward(data, x)

    cost = []
    for w in zip(*windows):
        idx = data.index[w[0]:w[1]]
        batch = data.loc[idx]

        with ProcessPoolExecutor(max_workers=cpu_count()) as p:
            fcn = partial(reward_local_cost, batch, rwd_name, gait)
            costs = p.map(fcn, product(alpha, gamma))
            cost += costs
    return np.mean(cost).item()


if __name__ == "__main__":

    args = parse_arguments()
    data, fpath = load_sequence_from_params(Pe=args.peclet, arp=args.action_random,
                                            path=args.datapath)
    print(f"Loaded data from {fpath}")

    ql_gammas = np.arange(*args.gamma_range)
    ql_alphas = np.arange(*args.alpha_range)

    # Evaluate on windows (batches)
    init_offset, window_length,  batch_offset = args.window
    data_length = data.shape[0] if args.data_length is None else args.data_length
    wins = overlapping_window_bounds(data_length, length=window_length,
                                             offset=init_offset,
                                             step=batch_offset)
    cost = partial(reward_cost, data=data, reward=reward_linear_combination,
                   alpha=ql_alphas, gamma=ql_gammas, windows=wins,
                   gait=swim_to_right.state_seq)

    if "result" not in locals():
        # Initial guess is proportional to intake accumulation
        #w0 = np.asarray([-0.5,-0.0,-0.0] + [1.0,0.0,0.0] + [-0.5,-0.0,-0.0])
        w0 = np.asarray([0.1] * 9)
        result = sco.minimize(cost, x0=w0, bounds=[(-1.0, 1.0)]*9, method='Nelder-Mead',
                              options=dict(fatol=0.01, adaptive=True))

    rwd_name = "reward_fcn"
    data[rwd_name] = reward_linear_combination(data, result.x)

    print(result)

    perf = []
    for i, ab in enumerate(zip(*wins)):
        idx = data.index[ab[0]:ab[1]]
        batch = data.loc[idx]
        # Evaluate

        def analize_batch(alpha_gamma):
            """Encapsulation of the analysis for a given batch. """
            a, g = alpha_gamma
            mql_ = runQL(batch, reward=rwd_name, alpha=a, gamma=g)
            gait_ = gait_from_mql(mql_, state0=swim_to_right.state_seq[0],
                                 action_fcn=contract_link)
            learned = np.array_equal(swim_to_right.state_seq, gait_)
            dist = gait_distance(gait_, swim_to_right.state_seq)
            gait_ = "".join(map(str, gait_))
            return dict(batch=i, learned=learned, gait=gait_, alpha=a, gamma=g,
                        distance=dist)

        with ProcessPoolExecutor(max_workers=cpu_count()) as p:
            results = p.map(analize_batch, product(ql_alphas, ql_gammas))
            perf += results

perf = pd.DataFrame.from_records(perf)

perf.groupby("batch").learned.mean().plot(kind="bar")

fig = plt.figure()
tmp = perf.groupby(["alpha", "gamma"]).learned.sum().unstack("gamma") / (perf.batch.max() + 1)
tmp.index = np.round(tmp.index, 2)
tmp.columns = np.round(tmp.columns, 2)
sns.heatmap(tmp, vmin=0, vmax=1)

plt.show()