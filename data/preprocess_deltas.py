""" Preprocess deltas raw files
"""
# Copyright (C) 2024 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
from pathlib import Path

import pandas as pd
import numpy as np
import fire

DELTAFOLDER = Path("Deltas")
COLUMNS = ("state", "action", "CoM_dX", "CoM_dY", "dangle", "viscous_diss_cum", "dX", "dY")


def deltas_raw_filepath(joints: int, angle: int):
    return DELTAFOLDER / f"{joints + 1}bodies_{angle}.txt"


def read_deltas_raw_file(*, joints: int, angle: int) -> pd.DataFrame:
    """Read raw file of deltas."""
    data = np.loadtxt(deltas_raw_filepath(joints, angle))

    nstates = 2 ** joints
    ndeltas = data.shape[0] // nstates
    df = pd.DataFrame()
    idx_state = np.arange(nstates)
    idx_action = np.arange(joints)
    for d in range(ndeltas):
        # We invert the order of the columns (action id), which coincides with
        # the position of the joint in the chain.
        # Originally the 0th joint was the left most (LSB to the left of the binary)
        # In the new order the 0th joint is the right most.
        df[COLUMNS[d+2]] = data[idx_state + d * nstates, ::-1].flatten()

    # Build state and action columns
    S, A = np.meshgrid(idx_state, idx_action)
    S = S.T.flatten()
    A = A.T.flatten()
    df[COLUMNS[0]] = S  # initial state
    df[COLUMNS[1]] = A  # joint id or action
    return df.set_index(list(COLUMNS[:2]))


def run(*, joints: int, angle: int):
    d = read_deltas_raw_file(joints=joints, angle=angle)
    fp = Path(f"deltas_{deltas_raw_filepath(joints, angle).stem}.pkl")
    d.to_pickle(fp)
    return fp


if __name__ == "__main__":
    fp = fire.Fire(run)
    deltas = pd.read_pickle(fp)
