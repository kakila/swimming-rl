# Data folder

This folder is a placeholder for data files.
Do not add data directly to the repository.

Lookup tables are to be stored in a folder `Deltas`.
The raw files must follow the naming convention

    "<# balls>bodies<joint-angle-in-deg>.txt"

Before you can use the scripts to load the deltas, you need to preprocess them
with the script `prerocess_delta.py`, for example

    python preprocess_deltas --nlinks=4 --angle=25

This will generate a Pandas dataframe on the pickle file printed by the command above

