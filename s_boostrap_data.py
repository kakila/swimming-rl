#!/usr/bin/env python3

# Copyright (C) 2022 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
import sys
from pathlib import Path
from itertools import product
from multiprocessing import cpu_count
from concurrent.futures import ProcessPoolExecutor
from functools import partial
import pandas as pd
import numpy as np

from ql import runQL, gait_from_mql, identify_final_gait, is_equal_rotation_invariant, rotation_equivalent
from sequence_filehandling import (Pe_to_D, sequence_filepath, load_sequence,
                                   action_sel_parameter)
from swimmer import (reward_displacement, reward_intake_increase,
                     reward_intake_accumulation, compute_rewards,
                     swim_to_right, contract_link)

from utils import overlapping_window_bounds

reward_functions = {
    "displacement": reward_displacement,
    "intake_increase": reward_intake_increase,
    "intake_accumulation": reward_intake_accumulation
    }


if __name__ == "__main__":

    # Sequence data folder is the first argument
    sequence_data_folder = Path(sys.argv[1])
    # Whether to run alpha-gamma grid in parallel is the second
    # If second present, then do not run parallel (len argv >= 3)
    parallel_loop = (len(sys.argv) == 2)

    data = []
    n_trajectories = 10
    arp = 0.5
    for trajid in range(n_trajectories):
        for Pe in Pe_to_D.keys():
            # File to load from
            seq_file = sequence_data_folder / sequence_filepath(pe=Pe, arp=arp, trajectory_id=trajid)
            print(f"Loading file {seq_file} ...")
            df_ = load_sequence(seq_file)
            # compute rewards
            df_ = compute_rewards(df_, rewards=reward_functions)
            # Add indicator columns
            df_["Pe"] = Pe
            df_[action_sel_parameter] = 0.5
            df_["TrID"] = trajid
            df_ = df_.reset_index()
            data.append(df_)

    data = pd.concat(data, ignore_index=True)

    ql_gammas = [0.999, 0.99, 0.95, 0.9, 0.7, 0.1]  # 0.96
    ql_alphas = [0.1, 0.3, 0.5, 0.8, 1.0]

    # Evaluate on windows (batches)
    window_length = 500
    init_offset = 50   # remove 50 iteration of transient
    batch_offset = 50  # move the window by 50 iterations
    data_length = 1000  # restrict the number of iterations
    w_ini, w_end = overlapping_window_bounds(data_length, length=window_length,
                                             offset=init_offset,
                                             step=batch_offset)
    MQL = []
    for tr_pe, d_pe in data.groupby(["TrID", "Pe"]):
        trajid, pe = tr_pe
        for i, ab in enumerate(zip(w_ini, w_end)):
            print(f"Evaluating batch {i}/{w_ini.size} (Pe: {pe}) ...")
            idx = d_pe.index[ab[0]:ab[1]]
            batch = d_pe.loc[idx]
            # Evaluate
            for rwd_name, rwd in reward_functions.items():
                print(f"    reward: {rwd_name}")


                def analize_batch(alpha_gamma):
                    """Encapsulation of the analysis for a given batch. """
                    a, g = alpha_gamma
                    mql_ = runQL(batch, reward=rwd_name, alpha=a, gamma=g)
                    gait = gait_from_mql(mql_, state0=swim_to_right.state_seq[0],
                                         action_fcn=contract_link)

                    mql_ = mql_.unstack().reset_index()
                    mql_.rename(columns={0: "Q_value"}, inplace=True)
                    mql_["alpha"] = a
                    mql_["gamma"] = g
                    mql_["gait"] = "".join(map(str, gait))
                    mql_["learned"] = np.array_equal(swim_to_right.state_seq, gait) # assumes that the reference gait is rotation equivalent (standarized)
                    mql_["batch"] = i
                    mql_["batch_ini"] = d_pe.iteration[idx[0]]
                    mql_["batch_end"] = d_pe.iteration[idx[-1]]
                    mql_["Pe"] = pe
                    mql_["reward"] = rwd_name
                    mql_["TrID"] = trajid
                    return mql_

                if parallel_loop:
                    with ProcessPoolExecutor(max_workers=cpu_count()) as p:
                        results = p.map(analize_batch, product(ql_alphas, ql_gammas))
                        MQL += results
                else:
                    for a_g in product(ql_alphas, ql_gammas):
                        mql_ = analize_batch(a_g)
                        MQL.append(mql_)

    MQL = pd.concat(MQL, ignore_index=True)
    MQL["TrID"] = MQL.TrID.astype("category")
    fout = Path("data") / f"bootstraped_ql_arp{arp}_ntraj{n_trajectories}.pkl"
    MQL.to_pickle(fout)
    print(f"Saved to {fout}")
