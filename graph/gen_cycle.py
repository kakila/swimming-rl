from random import choice

def gen_cycle(joints, *, state0: int = 0):
    path = [state0,]
    action = []
    ACT = set(range(joints));
    taken = set()
    is_cycle = False
    while True:
        # if there is an action already in the path and taken is empty
        if action and not taken:
            taken = {action[-1]}

        # available actions
        avail = list(ACT - taken)
        if avail:
            a_ = choice(avail)
        else:
            print("No more avialble actions!")
            break

        # Change state
        s_ = path[-1] ^ (1 << a_)
        # only accept new state if not in path
        if s_ not in path[1:]:
            path.append(s_)
            action.append(a_)
            # reset taken actions
            taken = set()
        else:
            # add action to taken actions
            taken.add(a_)
            continue

        is_cycle = path[-1] == path[0]  # a cycle!
        if is_cycle:  # a cycle!
            print("Cycle found!")
            break

    return is_cycle, path, action

