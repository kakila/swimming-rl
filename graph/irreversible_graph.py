""" Functions to analyze the graph of the states of a swimmer

    Here the swimmer cannot activate the same joint twice.
"""
# Copyright (C) 2024 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

from typing import TypeAlias
from functools import partial

import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import fire

State : TypeAlias = int
"""Type alias for states as int. """

StateT : TypeAlias = tuple[int, int]
"""Type alias for states as tuple. """


def valid_action(state_t: StateT, action: int) -> bool:
    """Whether the proposed action is valid.

       State is a tuple
    """
    return action != state_t[1]


def statet2state(joints: int, state_t: StateT) -> State:
    """Convert a state tuple to an integer state. """
    return (state_t[0] << joints) + (1 << state_t[1])


def state_config(joints: int, state: State) -> int:
    """Extract configuration from state as integer"""
    return state >> joints


def state_action(joints: int, state: State) -> int:
    """Extract action leading to joint configuration from state as integer"""
    return int(np.log2(state & (2 ** joints - 1)))


def state2statet(joints: int, state: State) -> StateT:
    """Convert a state integer to a tuple. """
    return state_config(joints, state), state_action(joints, state)


def statet2str(joints: int, state_t: StateT) -> str:
    """String representation of a tuple state """
    return f"{state_t[1]}-{format(state_t[0], f'0{joints}b')}"


def state2str(joints: int, state: State) -> str:
    """String representation of a integer state """
    return statet2str(joints, state2statet(joints, state))


def adjacency_graph(joints):
    """ Adjacency graph for a swimmer with given joints.

        Parameters
        -----------
        joints:
            Number of joints.

        Returns
        -------
        graph:
            Graph with joint states as nodes and transitions as edges.
    """
    joint_config = tuple(range(2 ** joints))
    actions = tuple(range(joints))
    to_node = partial(statet2state, joints)
    edges = []

    for J in joint_config:
        for Aprev in actions:
            state = (J, Aprev)
            node = to_node(state)
            for A in filter(partial(valid_action, state), actions):
                Jnext = J ^ (1 << A)
                e = (node, to_node((Jnext, A)))
                edges.append(e)
    g = nx.DiGraph(edges)
    return g


def run(joints: int, *, plot: bool = False):
    g = adjacency_graph(joints)  # joint state adjacency graph

    if plot:
        g_ = g.copy()
        nx.set_node_attributes(g_, {n: state_config(joints, n).bit_count() for n in g_.nodes()}, name="subset")
        g_ = nx.relabel_nodes(g_, {n: state2str(joints, n) for n in g_.nodes()})

        pos = nx.multipartite_layout(g_)
        # pos = nx.kamada_kawai_layout(g_)
        # pos = nx.spectral_layout(g_)
        nx.draw(g_, with_labels=True, node_color='lightgrey', node_size=(joints+2)*300,
                pos=pos)
        plt.show()


if __name__ == "__main__":
    fire.Fire(run)
