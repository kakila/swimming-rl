""" Functions to analyze the graph of the states ofa swimmer
"""
# Copyright (C) 2024 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

from pathlib import Path
from functools import partial
from collections.abc import Iterable, Sequence, Iterator
from typing import TypeAlias

import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import bsr_array

import networkx as nx
import fire

Node : TypeAlias = int
"""Type alias for nodes. """

Edge : TypeAlias = tuple[Node, Node]
"""Type alias for edges. """


def swimmer_graph(n_joints: int) -> nx.Graph:
    """ Adjacency graph for a swimmer with n_joints. 

        Parameters
        -----------
        n_joints:
            Number of joints.
        
        Returns
        -------
        graph:
            Graph with joint states as nodes and transitions as edges.
    """
    nodes = tuple(range(2**n_joints))
    edges = []
    for n in nodes:
        for k in range(n_joints):
            n_ = n ^ (1 << k)
            e = (n, n_)
            if e in edges or e[::-1] in edges:
                continue
            else:
                edges.append(e)
    g = nx.Graph(edges)
    return g


def node2bin(node: Node, *, bits: int) -> str: 
    """ Represent a joint state as a binary.

        The LSB is to the left of the binary.
    """
    return format(node, f"0{bits}b")


def nodes2bin(nodes: Iterable[Node], bits: int) -> Iterator[str]:
    """Iterator over joint states represented as binary. """
    return map(partial(node2bin, bits=bits), nodes)


def edges_index(E: Iterable[Edge]) -> dict[Edge, int]:
    """Mapping beteen edges (unsorted) and edge index.
    
        Returns
        -------
        E:
            Dictionry with edge as key and position in edges iterable as value.
            The edges in the eys are put in both order to search for undirected edges.
    """
    idx = {}
    for n, e in enumerate(E):
        idx[e] = n
        idx[e[::-1]] = n
    return idx


def cycle2eidx(e2idx: dict[Edge, int], c: Iterable[Node]) -> list[int]:
    """Convert a cycle to a list of edges in the cycle. 
    
        Parameters
        ----------
        e2idx:
            Mapping between edge and their index. See :func:`~edges_index`.
        c:
            A cycle as ordered collection of nodes that are visited by the cycles.
    """
    return [e2idx[e] for e in zip(c, c[1:]+[c[0]])]


def eidx2edges(ce: Iterable[int], edges: Sequence[Edge]) -> list[Edge]:
    """Convert edge index to edges. """  
    return [edges[n] for n in ce]


def cycle_eidx_sum(c1: Iterable[int], c2: Iterable[int]) -> list[int]:
    """Combine cycles (as edge index) to generate a new cycle."""
    c1nc2 = set(c1).intersection(set(c2)) 
    if c1nc2:
        return [e for e in c1+c2 if e not in c1nc2]
    else:
        return []


def cycle_eidx2cycle(ce: Iterable[int], edges: Sequence[Edge]):
    """Convert cycle as edge index to cycles as sequence of nodes.""" 
    e = eidx2edges(ce, edges)  # get edges in cycle
    C = nx.from_edgelist(e)    # build an undirected graph with those edges
    sc = list(nx.simple_cycles(C))  # get simple cycle. It should be only 1.
    if len(sc) != 1:
        raise ValueError("Edge list doesn't define a cycle.")
    return sc[0]


def cycle_sum(E: Iterable[Edge], c1: Iterable[Node], c2: Iterable[Node], 
              *, e2idx: dict[Edge, int] = None):
    """Combine cycles (as sequence of nodes) to generate a new cycle."""
    if e2idx is None:
        e2idx = edges_index(E)
    ce = list(map(partial(cycle2eidx, e2idx), [c1, c2]))
    ce = cycle_eidx_sum(*ce)
    return cycle_eidx2cycle(ce, E)


DATAFOLDER = Path("../data")


def cycle_basis_filepath(joints: int):
    return DATAFOLDER / f"cycle_basis_{joints}.txt"


def run(joints: int, *, plot: bool = False):
    g = swimmer_graph(joints)  # joint state adjacency graph
    c = sorted(nx.cycle_basis(g), key=len)  # cycle basis sorted by length

    # Print basis to a file
    fpath0 = cycle_basis_filepath(joints)
    n2b = partial(nodes2bin, bits=joints)
    txt = "\n".join(map(partial(str.join, ","), map(n2b, c)))
    fpath0.write_text(txt)

    # basis compatibility matrix
    # Indicates which cycles can be combined.
    # If the entry is nonzero the cycles can be combined
    # The nonzero value of the entry is the length of the cycle obtained by the combination
    E = list(g.edges())
    e2idx = edges_index(E)
    ce = list(map(partial(cycle2eidx, e2idx), c))
    row, col, clen = [], [], []
    for i, ci in enumerate(ce):
        ci_s = set(ci)
        for j, cj in enumerate(ce[i + 1:], i + 1):
            cincj = ci_s.intersection(set(cj))
            if cincj:
                row.append(i)
                col.append(j)
                clen.append(len(ci) + len(cj) - 2 * len(cincj))
    gcc = bsr_array((clen, (row, col)), shape=(len(ce),) * 2)
    cmax = []
    for i, j in zip(*np.nonzero(gcc == gcc.max())):
        cmax.append(cycle_sum(E, c[i], c[j], e2idx=e2idx))

    # Print combination with max length to a file
    fpath1 = DATAFOLDER / f"max_binary_cycle_{joints}.txt"
    txt = "\n".join(map(partial(str.join, ","), map(n2b, cmax)))
    fpath1.write_text(txt)

    if plot:
        g_ = g.copy()
        nx.set_node_attributes(g_, {n: n.bit_count() for n in g_.nodes()}, name="subset")
        g_ = nx.relabel_nodes(g_, {n: node2bin(n, bits=joints) for n in g_.nodes()})
        nx.draw(g_, pos=nx.multipartite_layout(g_), with_labels=True,
                node_color='lightgrey', node_size=joints*300)
        plt.show()

    return f"{fpath0}\n{fpath1}"


if __name__ == "__main__":
    fire.Fire(run)
