""" Functions to associate a sequence of states with a real number
"""
# Copyright (C) 2024 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
import operator

from collections.abc import Callable
from typing import TypeAlias
from functools import reduce, partial

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import networkx as nx
import fire

import swimmer_graph as sg


State: TypeAlias = int
"""State of the swimmer. """

Cycle: TypeAlias = list[State]
"""Cycle as sequence of states. """


def link2actions(joints: int) -> list[int]:
    return [2**l for l in range(joints)]


def states2action(s0: int, s1: int):
    """Convert state change to action.

        Returns the position of the joint in the swimmer that is activated
        to produce the change.
    """
    return int(np.log2(s0 ^ s1))


def cycle2action(c: Cycle):
    """Convert cycle to actions. """
    return [states2action(*ss) for ss in zip(c, list(c[1:]) + [c[0]])]


def eval_cycle(c: Cycle, *, value: Callable, op: Callable = operator.add):
    """ Associate a real number to the cycle.

        Parameters
        ----------
        c:
            A sequence of states.
        value:
            consumes state-action tuple and gives a real number.
        op:
            The binary operation to accumulate value.
    """
    return reduce(op, map(value, zip(c, cycle2action(c), strict=True)))


def deltas_filepath(joints: int, angle: int):
    return sg.DATAFOLDER / f"deltas_{joints+1}bodies_{angle}.pkl"


def eval_cycle_basis(*, joints: int, value: Callable, op: Callable = operator.add):
    g = sg.swimmer_graph(joints)  # joint state adjacency graph
    cycles = list(sorted(nx.cycle_basis(g), key=len))  # cycle basis sorted by length

    c2v = partial(eval_cycle, value=value, op=op)
    values = [c2v(c) for c in cycles]

    return cycles, values


def com_displacement(sa, dx_dy):
    return dx_dy.loc[sa].to_numpy().flatten()


def cycle_to_str(c: Cycle, joints: int):
    c0 = c[0]
    a = cycle2action(c)
    return f"{np.binary_repr(c0, width=joints)}: {'|'.join(map(str, a))}"


def run(*, joints: int, angle: int):
    deltas = pd.read_pickle(deltas_filepath(joints, angle))
    vcol = deltas[["CoM_dX", "CoM_dY"]]
    def value(sa): return com_displacement(sa, vcol)

    c, v = eval_cycle_basis(joints=joints, value=value)
    dCoM = np.sqrt(np.sum(np.asarray(v)**2, axis=1))
    cmax = np.argmax(dCoM)

    plt.plot(dCoM, 'o')
    plt.xlabel("cycle idx")
    plt.ylabel("CoM displacement length")
    plt.title(f"Winner {cycle_to_str(c[cmax], joints)}")
    plt.show()


if __name__ == "__main__":
    fire.Fire(run)
