# Copyright (C) 2022 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Stevens Paz

from typing import Union, Tuple
from pathlib import Path

import pandas as pd

Pe_to_D = dict(zip([0.06, 0.60, 6.00, 60.0, 2.00, 20.0],
                   ["100", "10", "1", "01", "3", "03"]))

action_sel_parameter = "prob_action_uniform"
"""Name of column for the action selector parameter (arp in file names)"""

sequence_CSV_fmt = dict(
        header=None,
        sep=" ",
    )

sequence_column_names = [
    "current state",
    "action",
    "next state",
    "center of mass displacement",
    "food uptake at the end of action sphere 1",
    "food uptake at the end of action sphere 2",
    "food uptake at the end of action sphere 3",
    "accumulated food uptake along action sphere 1",
    "accumulated food uptake along action sphere 2",
    "accumulated food uptake along action sphere 3",
    "food uptake at the start of action sphere 1",
    "food uptake at the start of action sphere 2",
    "food uptake at the start of action sphere 3",
]
sequence_column_names = [x.replace(" ", "_") for x in sequence_column_names]


def sequence_filepath(*, pe, arp, trajectory_id):
    """Path to sequence data file"""
    D = Pe_to_D[pe]
    return Path(f"Tr{trajectory_id:02d}-NoHLR-rand1.0-D{D}-arp{arp}") / "evolqlquantities.txt"


def load_sequence(filepath):
    """Load sequence data."""
    df = pd.read_csv(filepath, **sequence_CSV_fmt,
                     names=sequence_column_names)
    df.index.name = "iteration"
    for c in ["current_state", "action", "next_state"]:
        df[c] = df[c].astype("category")
    return df


def load_sequence_from_params(*, Pe: float, arp: float,
                              path: Union[str, Path] = '.') -> Tuple[pd.DataFrame, Path]:
    """Load sequence files with given parameters."""
    path = Path(path).resolve()
    # File to load from
    seq_file = path / sequence_filepath(pe=Pe, arp=arp)
    df_ = load_sequence(seq_file)
    # Add indicator columns
    df_["Pe"] = Pe
    df_[action_sel_parameter] = 0.5
    df_ = df_.reset_index()
    return df_, seq_file

